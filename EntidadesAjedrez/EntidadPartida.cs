﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadPartida
    {
        public int Idpartida { get; set; }
        public string Jornada { get; set; }
        public int Fkidarbitro { get; set; }
        public int Fkidhotel { get; set; }
        public int Fkidsala { get; set; }
        public int Entradas { get; set; }
        public EntidadPartida(int idpartida, string jornada,int fkaiarbitro, int fkidhotel, int fkidsala, int entradas)
        {
            Idpartida = idpartida;
            Jornada = jornada;
            Fkidarbitro = fkaiarbitro;
            Fkidhotel = fkidhotel;
            Fkidsala = fkidsala;
            Entradas = entradas;
        }
    }
}
