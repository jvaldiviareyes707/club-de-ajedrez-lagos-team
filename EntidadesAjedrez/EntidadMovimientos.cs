﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadMovimientos
    {
        public int Idpartida { get; set; }
        public int Idmovimiento { get; set; }
        public string Movimiento { get; set; }
        public string Comentario { get; set; }
        public EntidadMovimientos(int idpartida, int idmovimiento, string movimiento, string comentario)
        {
            Idpartida = idpartida;
            Idmovimiento = idmovimiento;
            Movimiento = movimiento;
            Comentario = comentario;
        }
    }
}
