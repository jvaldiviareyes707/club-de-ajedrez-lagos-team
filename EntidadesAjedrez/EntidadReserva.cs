﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadReserva
    {
        public int Idreserva { get; set; }
        public string Fechaentrada { get; set; }
        public string Fechasalida { get; set; }
        public int Fkidhotel { get; set; }
        public int Fkidparticipante { get; set; }
        public EntidadReserva(int idreserva, string fechaentrada, string fechasalida,int fkidhotel,int fkidparticipante)
        {
            Idreserva = idreserva;
            Fechaentrada = fechaentrada;
            Fechasalida = fechasalida;
            Fkidhotel = fkidhotel;
            Fkidparticipante = fkidparticipante;
        }
    }
}
