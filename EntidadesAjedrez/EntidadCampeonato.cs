﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadCampeonato
    {
        public int Idcampeonato { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int Fkidparticipante { get; set; }
        public EntidadCampeonato(int idcampeonato, string nombre, string tipo, int fkidparticipante)
        {
            Idcampeonato = idcampeonato;
            Nombre = nombre;
            Tipo = tipo;
            Fkidparticipante = fkidparticipante;
        }
    }
}
