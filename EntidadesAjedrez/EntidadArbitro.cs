﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadArbitro
    {
        public int IdArbitro { get; set; }
        public int Nsocio { get; set; }
        public EntidadArbitro(int idarbitro, int nsocio)
        {
            IdArbitro = idarbitro;
            Nsocio = nsocio;
        }
    }
}
