﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadParticipante
    {
        public int Nsocio { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public int Fkpais { get; set; }
        public EntidadParticipante(int nsocio, string nombre, string direccion, int fkpais)
        {
            Nsocio = nsocio;
            Nombre = nombre;
            Direccion = direccion;
            Fkpais = fkpais;
        }
    }
}
