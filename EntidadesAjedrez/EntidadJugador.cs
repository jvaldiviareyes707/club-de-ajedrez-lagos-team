﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
   public class EntidadJugador
    {
        public int Idjugador { get; set; }
        public int Nivel { get; set; }
        public int Nsocio { get; set; }
        public EntidadJugador(int idjugador, int nivel, int nsocio)
        {
            Idjugador = idjugador;
            Nivel = nivel;
            Nsocio = nsocio;
        }
    }
}
