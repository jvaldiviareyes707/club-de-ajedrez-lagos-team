﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadPais
    {
        public int Idpais { get; set; }
        public string Nombre { get; set; }
        public int NumeroClubs { get; set; }
        public int Representa { get; set; }
        public EntidadPais(int idpais, string nombre, int numeroclubs, int representa)
        {
            Idpais = idpais;
            Nombre= nombre;
            NumeroClubs = numeroclubs;
            Representa = representa;
        }
    }
}
