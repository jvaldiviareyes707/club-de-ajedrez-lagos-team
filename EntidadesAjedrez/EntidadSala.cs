﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadSala
    {
        public int IDSala { get; set; }
        public int Capacidad { get; set; }
        public string Medios { get; set; }
        public int Idhotel { get; set; }
        public EntidadSala(int sala, int capacidad, string medios,int idhotel)
        {
            IDSala = sala;
            Capacidad = capacidad;
            Medios = medios;
            Idhotel = idhotel;
        }
    }
}
