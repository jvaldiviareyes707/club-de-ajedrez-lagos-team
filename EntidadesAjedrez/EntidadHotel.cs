﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadHotel
    {
        public int IdHotel { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public EntidadHotel(int idhotel, string nombre, string direccion, string telefono)
        {
            IdHotel = idhotel;
            Nombre = nombre;
            Direccion = direccion;
            Telefono = telefono;
        }
    }
}
