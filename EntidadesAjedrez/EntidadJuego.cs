﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadesAjedrez
{
    public class EntidadJuego
    {
        public int Idpartida { get; set; }
        public int Idjugador { get; set; }
        public string Color { get; set; }
        public EntidadJuego(int idpartida, int idjugador, string color)
        {
            Idpartida = idpartida;
            Idjugador = idjugador;
            Color = color;
        }
    }
}
