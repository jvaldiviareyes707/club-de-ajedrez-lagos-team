Create DATABASE Ajedrez;
CREATE TABLE Hotel (
  `idHotel` INT AUTO_INCREMENT,
  `nombre` VARCHAR(45) ,
  `direccion` VARCHAR(45) ,
  `telefono` VARCHAR(45) ,
  PRIMARY KEY (`idHotel`));


-- -----------------------------------------------------
-- Table `Ajedrez`.`sala`
-- -----------------------------------------------------
CREATE TABLE sala (
  `idsala` INT AUTO_INCREMENT,
  `capacidad` INT ,
  `medios` VARCHAR(45),
  `idhotel` INT,
  PRIMARY KEY (`idsala`),
    FOREIGN KEY (`idhotel`)
    REFERENCES Ajedrez.Hotel(idHotel));


-- -----------------------------------------------------
-- Table `Ajedrez`.`pais`
-- -----------------------------------------------------
CREATE TABLE pais(
  `idpais` INT AUTO_INCREMENT,
  `nombre` VARCHAR(45),
  `numeroClubs` INT ,
  `representa` INT,
  PRIMARY KEY (`idpais`));


-- -----------------------------------------------------
-- Table `Ajedrez`.`participante`
-- -----------------------------------------------------
CREATE TABLE participante (
  `n.socio` INT AUTO_INCREMENT,
  `nombre` VARCHAR(45) ,
  `direccion` VARCHAR(45),
  `fkpais` INT,
  PRIMARY KEY (`n.socio`),
    FOREIGN KEY (`fkpais`)
    REFERENCES Ajedrez.pais(`idpais`)
    );


-- -----------------------------------------------------
-- Table `Ajedrez`.`arbitro`
-- -----------------------------------------------------
CREATE TABLE arbitro (
  `idarbitro` INT AUTO_INCREMENT,
  `n.socio` INT,
  PRIMARY KEY (`idarbitro`),
    FOREIGN KEY (`n.socio`)
    REFERENCES participante(`n.socio`)
    );


-- -----------------------------------------------------
-- Table `Ajedrez`.`jugador`
-- -----------------------------------------------------
CREATE TABLE jugador (
  `idjugador` INT  AUTO_INCREMENT,
  `nivel` INT ,
  `n.socio` INT,
  PRIMARY KEY (`idjugador`),
    FOREIGN KEY (`n.socio`)
    REFERENCES participante (`n.socio`));


-- -----------------------------------------------------
-- Table `Ajedrez`.`reserva`
-- -----------------------------------------------------
CREATE TABLE reserva (
  `idreserva` INT AUTO_INCREMENT,
  `fechaentrada` VARCHAR(45),
  `fechasalida` VARCHAR(45),
  `fkidhotel` INT,
  `fkidparticipante` INT,
  PRIMARY KEY (`idreserva`),
    FOREIGN KEY (`fkidhotel`)
    REFERENCES Hotel (`idHotel`),
    FOREIGN KEY (`fkidparticipante`)
    REFERENCES participante (`n.socio`));


-- -----------------------------------------------------
-- Table `Ajedrez`.`campeonato`
-- -----------------------------------------------------
CREATE TABLE campeonato (
  `idcampeonato` INT  AUTO_INCREMENT,
  `nombre` VARCHAR(45) ,
  `tipo` VARCHAR(45) ,
  `fkidparticipante` INT ,
  PRIMARY KEY (`idcampeonato`),
    FOREIGN KEY (`fkidparticipante`)
    REFERENCES participante (`n.socio`));


-- -----------------------------------------------------
-- Table `Ajedrez`.`partida`
-- -----------------------------------------------------
CREATE TABLE partida (
  `idpartida` INT AUTO_INCREMENT,
  `jornada` VARCHAR(45),
  `fkidarbitro` INT,
  `fkidhotel` INT,
  `fkidsala` INT ,
  `entradas` INT ,
  PRIMARY KEY (`idpartida`),
    FOREIGN KEY (`fkidarbitro`)
    REFERENCES arbitro (`idarbitro`),
    FOREIGN KEY (`fkidsala`)
    REFERENCES sala (`idsala`),
    FOREIGN KEY (`fkidhotel`)
    REFERENCES `Ajedrez`.`Hotel` (`idHotel`)
    );


-- -----------------------------------------------------
-- Table `Ajedrez`.`juego`
-- -----------------------------------------------------
CREATE TABLE juego (
  `idpartida` INT ,
  `idjugador` INT ,
  `color` VARCHAR(45) ,
  PRIMARY KEY (`idpartida`, `idjugador`),
    FOREIGN KEY (`idpartida`)
    REFERENCES `partida` (`idpartida`),
    FOREIGN KEY (`idjugador`)
    REFERENCES `jugador` (`idjugador`)
    );


-- -----------------------------------------------------
-- Table `Ajedrez`.`movimientos`
-- -----------------------------------------------------
CREATE TABLE `movimientos` (
  `idpartida` INT ,
  `idmovimiento` INT AUTO_INCREMENT,
  `movimiento` VARCHAR(45),
  `comentario` VARCHAR(45),
  PRIMARY KEY (`idmovimiento`, `idpartida`),
    FOREIGN KEY (`idpartida`)
    REFERENCES `Ajedrez`.`partida` (`idpartida`));