﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;

namespace Manejador
{
   public class ManejadorJugador
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadJugador jugador)
        {
            return ce.Comando(string.Format("insert into jugador values(" +
            "null,{0},{1})", jugador.Nivel,jugador.Nsocio));
        }
        public string Borrar(EntidadJugador jugador)
        {
            return ce.Comando(string.Format("delete from jugador where idjugador={0}", jugador.Idjugador));
        }
        public string Modificar(EntidadJugador jugador)
        {
            return ce.Comando(string.Format("update jugador set nivel={0}, n.socio={1}" , jugador.Nivel,jugador.Nsocio));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
