﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;

namespace Manejador
{
    public class ManejadorJuego
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadJuego juego)
        {
            return ce.Comando(string.Format("insert into juego values(" +
            "{0},{1},'{2}')", juego.Idpartida, juego.Idjugador, juego.Color));
        }
        public string Borrar(EntidadJuego juego)
        {
            return ce.Comando(string.Format("delete from juego where idprestamos={0}", juego.Idpartida));
        }
        public string Modificar(EntidadJuego juego)
        {
            return ce.Comando(string.Format("update juego set idjugador={0}, color='{1}'" +
                "where idpartida={2}", juego.Idjugador, juego.Color, juego.Idpartida));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
