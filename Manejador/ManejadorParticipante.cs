﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;

namespace Manejador
{
   public class ManejadorParticipante
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadParticipante participante)
        {
            return ce.Comando(string.Format("insert into participante values(" +
            "null,'{0}','{1}',{2})", participante.Nombre, participante.Direccion, participante.Fkpais));
        }
        public string Borrar(EntidadParticipante participante)
        {
            return ce.Comando(string.Format("delete from participante where n.socio={0}", participante.Nsocio));
        }
        public string Modificar(EntidadParticipante participante)
        {
            return ce.Comando(string.Format("update participante set nombre='{0}', direccion='{1}',fkpais='{2}'" +
                "where n.socio={3}", participante.Nombre, participante.Direccion, participante.Fkpais, participante.Nsocio));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
