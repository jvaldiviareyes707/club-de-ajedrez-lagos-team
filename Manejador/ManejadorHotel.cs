﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntidadesAjedrez;
using AccesoDatos;
using System.Data;

namespace Manejador
{
    public class ManejadorHotel
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadHotel Hotel)
        {
            return ce.Comando(string.Format("insert into hotel values(" +
            "null,'{0}','{1}','{2}')", Hotel.Nombre, Hotel.Direccion, Hotel.Telefono, Hotel.IdHotel));
        }
        public string Borrar(EntidadHotel Hotel)
        {
            return ce.Comando(string.Format("delete from hotel where idHotel={0}", Hotel.IdHotel));
        }
        public string Modificar(EntidadHotel Hotel)
        {
            return ce.Comando(string.Format("update hotel set nombre='{0}', direccion='{1}',telefono='{2}'" +
                "where idHotel={3}", Hotel.Nombre, Hotel.Direccion, Hotel.Telefono, Hotel.IdHotel));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
