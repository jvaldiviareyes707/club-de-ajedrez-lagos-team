﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;

namespace Manejador
{
    public class ManejadorPartida
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadPartida partida)
        {
            return ce.Comando(string.Format("insert into partida values(" +
            "null,'{0}',{1},{2},{3},{4})", partida.Jornada, partida.Fkidarbitro, partida.Fkidhotel, partida.Fkidsala, partida.Entradas));
        }
        public string Borrar(EntidadPartida partida)
        {
            return ce.Comando(string.Format("delete from partida where idpartida={0}", partida.Idpartida));
        }
        public string Modificar(EntidadPartida partida)
        {
            return ce.Comando(string.Format("update partida set jornada='{0}', fkidarbitro={1},fkidhotel={2},fksala={3},entradas={4}" +
                "where idpartida={5}", partida.Jornada, partida.Fkidarbitro, partida.Fkidhotel, partida.Fkidsala, partida.Entradas, partida.Idpartida));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
