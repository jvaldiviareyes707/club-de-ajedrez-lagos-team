﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;



namespace Manejador
{
    public class ManejadorSala
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadSala sala)
        {
            return ce.Comando(string.Format("insert into sala values(" +
            "null,{0},'{1}',{2})", sala.Capacidad, sala.Medios, sala.Idhotel));
        }
        public string Borrar(EntidadSala sala)
        {
            return ce.Comando(string.Format("delete from sala where idsala={0}", sala.IDSala));
        }
        public string Modificar(EntidadSala sala)
        {
            return ce.Comando(string.Format("update sala set capacidad={0}, medios='{1}',idhotel={2}" +
                "where idsala={3}", sala.Capacidad, sala.Medios, sala.Idhotel, sala.IDSala));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
