﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;

namespace Manejador
{
    public class ManejadorCampeonato
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadCampeonato Campeonato)
        {
            return ce.Comando(string.Format("insert into Campeonato values(" +
            "null,'{0}','{1}',{2})", Campeonato.Nombre,Campeonato.Tipo,Campeonato.Fkidparticipante));
        }
        public string Borrar(EntidadCampeonato Campeonato)
        {
            return ce.Comando(string.Format("delete from campeonato where idcampeonato={0}", Campeonato.Idcampeonato));
        }
        public string Modificar(EntidadCampeonato Campeonato)
        {
            return ce.Comando(string.Format("update campeonato set nombre='{0}',tipo='{1},fkidparticipante={2} " +
                "where idcampeonato={3}", Campeonato.Nombre, Campeonato.Tipo, Campeonato.Tipo, Campeonato.Fkidparticipante, Campeonato.Idcampeonato));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
