﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;


namespace Manejador
{
  public  class ManejadorReserva
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadReserva reserva)
        {
            return ce.Comando(string.Format("insert into reserva values(" +
            "null,'{0}','{1}',{2},{3})", reserva.Fechaentrada,reserva.Fechasalida, reserva.Fkidhotel, reserva.Fkidparticipante));
        }
        public string Borrar(EntidadReserva reserva)
        {
            return ce.Comando(string.Format("delete from reserva where idreserva={0}", reserva.Idreserva));
        }
        public string Modificar(EntidadReserva reserva)
        {
            return ce.Comando(string.Format("update reserva set fechaentrada='{0}', fechasalida='{1}',fkidhotel={2},fkidparticipante={3}" +
                "where idreserva={4}", reserva.Fechaentrada, reserva.Fechasalida, reserva.Fkidhotel, reserva.Fkidparticipante));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
