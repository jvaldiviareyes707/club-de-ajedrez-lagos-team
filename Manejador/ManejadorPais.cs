﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;

namespace Manejador
{
   public class ManejadorPais
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadPais pais)
        {
            return ce.Comando(string.Format("insert into pais values(" +
            "null,'{0}',{1},{2})", pais.Nombre, pais.NumeroClubs, pais.Representa));
        }
        public string Borrar(EntidadPais pais)
        {
            return ce.Comando(string.Format("delete from pais where idpais={0}", pais.Idpais));
        }
        public string Modificar(EntidadPais pais)
        {
            return ce.Comando(string.Format("update pais set nombre='{0}', numeroClubs={1},representa={2}" +
                "where idpais={3}", pais.Nombre, pais.NumeroClubs, pais.Representa, pais.Representa));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
