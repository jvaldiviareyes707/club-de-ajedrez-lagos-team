﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;

namespace Manejador
{
    public class ManejadorArbitro
    {
        Conexion ce = new Conexion();
        public string Guardar(EntidadArbitro arbitro)
        {
            return ce.Comando(string.Format("insert into arbitro values(" +
            "null,'{0}')", arbitro.Nsocio));
        }
        public string Borrar(EntidadArbitro arbitro)
        {
            return ce.Comando(string.Format("delete from arbitro where idarbitro={0}", arbitro.IdArbitro));
        }
        public string Modificar(EntidadArbitro arbitro)
        {
            return ce.Comando(string.Format("update arbitro set n.socio='{0}' " +
                "where idarbitro={1}", arbitro.Nsocio, arbitro.IdArbitro));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
