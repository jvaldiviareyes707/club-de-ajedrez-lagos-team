﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using EntidadesAjedrez;


namespace Manejador
{
    public class ManejadorMovimientos
    {
        Conexion ce = new Conexion();


        public string Guardar(EntidadMovimientos movimientos)
        {
            return ce.Comando(string.Format("insert into movimientos values(" +
            "{0},{1},'{2}','{3}')", movimientos.Idpartida,movimientos.Idmovimiento, movimientos.Movimiento, movimientos.Comentario));
        }
        public string Borrar(EntidadMovimientos movimientos)
        {
            return ce.Comando(string.Format("delete from movimientos where idmovimiento={0}", movimientos.Idmovimiento));
        }
        public string Modificar(EntidadMovimientos movimientos)
        {
            return ce.Comando(string.Format("update movimientos set idpartida={0}, movimiento='{1}',comentario='{2}'" +
                "where idmovimiento={3}", movimientos.Idpartida, movimientos.Movimiento, movimientos.Comentario, movimientos.Idmovimiento));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ce.Mostrar(q, tabla);
        }
    }
}
