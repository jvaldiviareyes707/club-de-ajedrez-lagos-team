﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejador;
using EntidadesAjedrez;

namespace Presentacion
{
    public partial class FrmJugador : Form
    {
        ManejadorJugador ma;
        public FrmJugador()
        {
            InitializeComponent();
            ma = new ManejadorJugador();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            FrmParticipantes a = new FrmParticipantes();
            a.Show();
            Close();
        }
        void actualizar()
        {
            dtgAlumnos.DataSource = ma.Listado(string.Format("" +
                "select * from jugador"), "jugador").Tables[0];
            dtgAlumnos.AutoResizeColumns();
            dtgAlumnos.Columns[1].ReadOnly = true;
        }

        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Borrar(new EntidadJugador(int.Parse(TxtId.Text), 0,0));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnAgreagar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Guardar(new EntidadJugador(0,int.Parse(TxtNivel.Text),int.Parse(TxtNSOcio.Text) ));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Modificar(new EntidadJugador(int.Parse(TxtId.Text), int.Parse(TxtNivel.Text), int.Parse(TxtNSOcio.Text))); 

                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void FrmJugador_Load(object sender, EventArgs e)
        {
            actualizar();
        }
    }
}
