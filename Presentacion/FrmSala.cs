﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejador;
using EntidadesAjedrez;

namespace Presentacion
{
    public partial class FrmSala : Form
    {
        ManejadorSala ma;
        public FrmSala()
        {
            InitializeComponent();
            ma = new ManejadorSala();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
           
            Close();
        }
        void actualizar()
        {
            dtgAlumnos.DataSource = ma.Listado(string.Format("" +
                "select * from sala"), "sala").Tables[0];
            dtgAlumnos.AutoResizeColumns();
            dtgAlumnos.Columns[1].ReadOnly = true;
        }
        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Borrar(new EntidadSala(int.Parse(TxtId.Text),0, "",0));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnAgreagar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Guardar(new EntidadSala(0,int.Parse(TxtCapacidad.Text),TxtMedios.Text,int.Parse(TxtHotel.Text)));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Modificar(new EntidadSala(int.Parse(TxtId.Text), int.Parse(TxtCapacidad.Text), TxtMedios.Text, int.Parse(TxtHotel.Text)));

                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void FrmSala_Load(object sender, EventArgs e)
        {
            actualizar();
        }
    }
}
