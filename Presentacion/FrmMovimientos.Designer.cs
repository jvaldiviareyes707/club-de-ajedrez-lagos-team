﻿namespace Presentacion
{
    partial class FrmMovimientos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolver = new System.Windows.Forms.Button();
            this.BtnModificar = new System.Windows.Forms.Button();
            this.BtnAgreagar = new System.Windows.Forms.Button();
            this.TxtIDMovimiento = new System.Windows.Forms.TextBox();
            this.TxtIdpartida = new System.Windows.Forms.TextBox();
            this.BtnBorrar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtgAlumnos = new System.Windows.Forms.DataGridView();
            this.TxtMovimiento = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TXTComentario = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(5, 13);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(4);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(100, 28);
            this.btnVolver.TabIndex = 17;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // BtnModificar
            // 
            this.BtnModificar.Location = new System.Drawing.Point(497, 296);
            this.BtnModificar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.Size = new System.Drawing.Size(100, 28);
            this.BtnModificar.TabIndex = 16;
            this.BtnModificar.Text = "Modificar";
            this.BtnModificar.UseVisualStyleBackColor = true;
            this.BtnModificar.Click += new System.EventHandler(this.BtnModificar_Click);
            // 
            // BtnAgreagar
            // 
            this.BtnAgreagar.Location = new System.Drawing.Point(357, 311);
            this.BtnAgreagar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAgreagar.Name = "BtnAgreagar";
            this.BtnAgreagar.Size = new System.Drawing.Size(100, 28);
            this.BtnAgreagar.TabIndex = 15;
            this.BtnAgreagar.Text = "Agregar";
            this.BtnAgreagar.UseVisualStyleBackColor = true;
            this.BtnAgreagar.Click += new System.EventHandler(this.BtnAgreagar_Click);
            // 
            // TxtIDMovimiento
            // 
            this.TxtIDMovimiento.Location = new System.Drawing.Point(221, 307);
            this.TxtIDMovimiento.Margin = new System.Windows.Forms.Padding(4);
            this.TxtIDMovimiento.Name = "TxtIDMovimiento";
            this.TxtIDMovimiento.Size = new System.Drawing.Size(132, 22);
            this.TxtIDMovimiento.TabIndex = 14;
            // 
            // TxtIdpartida
            // 
            this.TxtIdpartida.Location = new System.Drawing.Point(221, 275);
            this.TxtIdpartida.Margin = new System.Windows.Forms.Padding(4);
            this.TxtIdpartida.Name = "TxtIdpartida";
            this.TxtIdpartida.Size = new System.Drawing.Size(132, 22);
            this.TxtIdpartida.TabIndex = 13;
            // 
            // BtnBorrar
            // 
            this.BtnBorrar.Location = new System.Drawing.Point(357, 275);
            this.BtnBorrar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnBorrar.Name = "BtnBorrar";
            this.BtnBorrar.Size = new System.Drawing.Size(100, 28);
            this.BtnBorrar.TabIndex = 12;
            this.BtnBorrar.Text = "Borrar";
            this.BtnBorrar.UseVisualStyleBackColor = true;
            this.BtnBorrar.Click += new System.EventHandler(this.BtnBorrar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(122, 307);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "IDMovimiento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 275);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "IDPartida";
            // 
            // dtgAlumnos
            // 
            this.dtgAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgAlumnos.Location = new System.Drawing.Point(158, 13);
            this.dtgAlumnos.Margin = new System.Windows.Forms.Padding(4);
            this.dtgAlumnos.Name = "dtgAlumnos";
            this.dtgAlumnos.Size = new System.Drawing.Size(439, 233);
            this.dtgAlumnos.TabIndex = 9;
            // 
            // TxtMovimiento
            // 
            this.TxtMovimiento.Location = new System.Drawing.Point(221, 337);
            this.TxtMovimiento.Margin = new System.Windows.Forms.Padding(4);
            this.TxtMovimiento.Name = "TxtMovimiento";
            this.TxtMovimiento.Size = new System.Drawing.Size(132, 22);
            this.TxtMovimiento.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(135, 337);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "Movimiento";
            // 
            // TXTComentario
            // 
            this.TXTComentario.Location = new System.Drawing.Point(221, 367);
            this.TXTComentario.Margin = new System.Windows.Forms.Padding(4);
            this.TXTComentario.Name = "TXTComentario";
            this.TXTComentario.Size = new System.Drawing.Size(132, 22);
            this.TXTComentario.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(134, 367);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "Comentario";
            // 
            // FrmMovimientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TXTComentario);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtMovimiento);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.BtnModificar);
            this.Controls.Add(this.BtnAgreagar);
            this.Controls.Add(this.TxtIDMovimiento);
            this.Controls.Add(this.TxtIdpartida);
            this.Controls.Add(this.BtnBorrar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgAlumnos);
            this.Name = "FrmMovimientos";
            this.Text = "FrmMovimientos";
            this.Load += new System.EventHandler(this.FrmMovimientos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Button BtnModificar;
        private System.Windows.Forms.Button BtnAgreagar;
        private System.Windows.Forms.TextBox TxtIDMovimiento;
        private System.Windows.Forms.TextBox TxtIdpartida;
        private System.Windows.Forms.Button BtnBorrar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dtgAlumnos;
        private System.Windows.Forms.TextBox TxtMovimiento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TXTComentario;
        private System.Windows.Forms.Label label4;
    }
}