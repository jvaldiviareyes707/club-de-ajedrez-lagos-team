﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejador;
using EntidadesAjedrez;

namespace Presentacion
{
    public partial class FrmReserva : Form
    {
        ManejadorReserva ma;
        public FrmReserva()
        {
            InitializeComponent();
            ma = new ManejadorReserva();
        }
        void actualizar()
        {
            dtgAlumnos.DataSource = ma.Listado(string.Format("" +
                "select * from reserva"), "reserva").Tables[0];
            dtgAlumnos.AutoResizeColumns();
            dtgAlumnos.Columns[1].ReadOnly = true;
        }
        private void btnVolver_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Borrar(new EntidadReserva(int.Parse(TxtId.Text), "", "",0,0));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnAgreagar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Guardar(new EntidadReserva(0, TxtEntrada.Text,TxtSalida.Text,int.Parse(TXTHotel.Text),int.Parse(TXTParticipante.Text)));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Modificar(new EntidadReserva(int.Parse(TxtId.Text), TxtEntrada.Text, TxtSalida.Text, int.Parse(TXTHotel.Text), int.Parse(TXTParticipante.Text)));

                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void FrmReserva_Load(object sender, EventArgs e)
        {
            actualizar();
        }
    }
}
