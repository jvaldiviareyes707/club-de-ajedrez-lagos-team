﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejador;
using EntidadesAjedrez;

namespace Presentacion
{
    public partial class FrmJuego : Form
    {
        ManejadorJuego ma;
        public FrmJuego()
        {
            InitializeComponent();
            ma = new ManejadorJuego();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            
            Close();
        }
        void actualizar()
        {
            dtgAlumnos.DataSource = ma.Listado(string.Format("" +
                "select * from juego"), "juego").Tables[0];
            dtgAlumnos.AutoResizeColumns();
            dtgAlumnos.Columns[1].ReadOnly = true;
        }

        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Borrar(new EntidadJuego(int.Parse(TxtId.Text),0, ""));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnAgreagar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Guardar(new EntidadJuego(int.Parse(TxtId.Text),int.Parse(TxtIDJugador.Text), TxtColor.Text));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Modificar(new EntidadJuego(int.Parse(TxtId.Text), int.Parse(TxtIDJugador.Text), TxtColor.Text));

                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void FrmJuego_Load(object sender, EventArgs e)
        {
            actualizar();
        }
    }
}
