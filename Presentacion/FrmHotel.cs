﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntidadesAjedrez;
using Manejador;

namespace Presentacion
{
    public partial class FrmHotel : Form
    {
        ManejadorHotel ma;
        public FrmHotel()
        {
            InitializeComponent();
            ma = new ManejadorHotel();

        }
        void actualizar()
        {
            dtgAlumnos.DataSource = ma.Listado(string.Format("" +
                "select * from hotel"), "hotel").Tables[0];
            dtgAlumnos.AutoResizeColumns();
            dtgAlumnos.Columns[1].ReadOnly = true;
        }
        private void btnVolver_Click(object sender, EventArgs e)
        {
            
            Close();
        }

        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Borrar(new EntidadHotel(int.Parse(TxtId.Text), "", "", ""));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnAgreagar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Guardar(new EntidadHotel(0, TxtNombre.Text,txtDireccion.Text,Txttelefono.Text));
               
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void FrmHotel_Load(object sender, EventArgs e)
        {
            actualizar();
        }
    }
}
