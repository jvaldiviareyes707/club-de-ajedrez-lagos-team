﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejador;
using EntidadesAjedrez;

namespace Presentacion
{
    public partial class FrmParticipantes : Form
    {
        ManejadorParticipante mc;
        public FrmParticipantes()
        {
            InitializeComponent();
            mc = new ManejadorParticipante();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmArbitro a = new frmArbitro();
            a.Show();
            Close();
        }

        private void btnvolver_Click(object sender, EventArgs e)
        {
            
            Close();
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mc.Borrar(new EntidadParticipante(int.Parse(TxtCodigo.Text), "", "", 0));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }
        void actualizar()
        {
            dataGridView1.DataSource = mc.Listado(string.Format("" +
                "select * from participante"), "participante").Tables[0];
            dataGridView1.AutoResizeColumns();
            dataGridView1.Columns[1].ReadOnly = true;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mc.Guardar(new EntidadParticipante(0, TxtFechadeprestamo.Text, txtfechadeentrega.Text, int.Parse(TxtAlumno.Text)));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mc.Modificar(new EntidadParticipante(int.Parse(TxtCodigo.Text), TxtFechadeprestamo.Text, txtfechadeentrega.Text, int.Parse(TxtAlumno.Text)));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void FrmParticipantes_Load(object sender, EventArgs e)
        {
            actualizar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmJugador a = new FrmJugador();
            a.Show();
            Close();
        }
    }
}
