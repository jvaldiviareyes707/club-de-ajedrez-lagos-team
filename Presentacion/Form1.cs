﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejador;
using EntidadesAjedrez;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        ManejadorCampeonato ma;
        public Form1()
        {
            InitializeComponent();
            ma = new ManejadorCampeonato();
        }
        void actualizar()
        {
            dtgAlumnos.DataSource = ma.Listado(string.Format("" +
                "select * from campeonato"), "campeonato").Tables[0];
            dtgAlumnos.AutoResizeColumns();
            dtgAlumnos.Columns[1].ReadOnly = true;
        }
        private void btnVolver_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Borrar(new EntidadCampeonato(int.Parse(TxtId.Text), "", "", 0));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnAgreagar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Guardar(new EntidadCampeonato(0, TxtEntrada.Text, TxtSalida.Text, int.Parse(TXTParticipante.Text)));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Modificar(new EntidadCampeonato(int.Parse(TxtId.Text), TxtEntrada.Text, TxtSalida.Text, int.Parse(TXTParticipante.Text)));

                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            actualizar();
        }
    }
}
