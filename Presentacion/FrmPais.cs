﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejador;
using EntidadesAjedrez;

namespace Presentacion
{
    public partial class FrmPais : Form
    {
        ManejadorPais ma;
        public FrmPais()
        {
            InitializeComponent();
            ma = new ManejadorPais();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {            
            Close();
        }

        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Borrar(new EntidadPais(int.Parse(TxtId.Text), "",0,0));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }
        void actualizar()
        {
            dtgAlumnos.DataSource = ma.Listado(string.Format("" +
                "select * from pais"), "pais").Tables[0];
            dtgAlumnos.AutoResizeColumns();
            dtgAlumnos.Columns[1].ReadOnly = true;
        }

        private void BtnAgreagar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Guardar(new EntidadPais(0, TxtNombre.Text,int.Parse(TxtNumClubs.Text),int.Parse(TXTRepresenta.Text)));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Modificar(new EntidadPais(int.Parse(TxtId.Text), TxtNombre.Text,int.Parse(TxtNumClubs.Text),int.Parse(TXTRepresenta.Text)));

                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void FrmPais_Load(object sender, EventArgs e)
        {
            actualizar();
        }
    }
}
