﻿namespace Presentacion
{
    partial class FrmReserva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolver = new System.Windows.Forms.Button();
            this.BtnModificar = new System.Windows.Forms.Button();
            this.BtnAgreagar = new System.Windows.Forms.Button();
            this.TxtEntrada = new System.Windows.Forms.TextBox();
            this.TxtId = new System.Windows.Forms.TextBox();
            this.BtnBorrar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtgAlumnos = new System.Windows.Forms.DataGridView();
            this.TxtSalida = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TXTHotel = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TXTParticipante = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(3, 13);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(4);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(100, 28);
            this.btnVolver.TabIndex = 17;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // BtnModificar
            // 
            this.BtnModificar.Location = new System.Drawing.Point(495, 296);
            this.BtnModificar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.Size = new System.Drawing.Size(100, 28);
            this.BtnModificar.TabIndex = 16;
            this.BtnModificar.Text = "Modificar";
            this.BtnModificar.UseVisualStyleBackColor = true;
            this.BtnModificar.Click += new System.EventHandler(this.BtnModificar_Click);
            // 
            // BtnAgreagar
            // 
            this.BtnAgreagar.Location = new System.Drawing.Point(355, 311);
            this.BtnAgreagar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAgreagar.Name = "BtnAgreagar";
            this.BtnAgreagar.Size = new System.Drawing.Size(100, 28);
            this.BtnAgreagar.TabIndex = 15;
            this.BtnAgreagar.Text = "Agregar";
            this.BtnAgreagar.UseVisualStyleBackColor = true;
            this.BtnAgreagar.Click += new System.EventHandler(this.BtnAgreagar_Click);
            // 
            // TxtEntrada
            // 
            this.TxtEntrada.Location = new System.Drawing.Point(219, 307);
            this.TxtEntrada.Margin = new System.Windows.Forms.Padding(4);
            this.TxtEntrada.Name = "TxtEntrada";
            this.TxtEntrada.Size = new System.Drawing.Size(132, 22);
            this.TxtEntrada.TabIndex = 14;
            // 
            // TxtId
            // 
            this.TxtId.Location = new System.Drawing.Point(219, 275);
            this.TxtId.Margin = new System.Windows.Forms.Padding(4);
            this.TxtId.Name = "TxtId";
            this.TxtId.Size = new System.Drawing.Size(132, 22);
            this.TxtId.TabIndex = 13;
            // 
            // BtnBorrar
            // 
            this.BtnBorrar.Location = new System.Drawing.Point(355, 275);
            this.BtnBorrar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnBorrar.Name = "BtnBorrar";
            this.BtnBorrar.Size = new System.Drawing.Size(100, 28);
            this.BtnBorrar.TabIndex = 12;
            this.BtnBorrar.Text = "Borrar";
            this.BtnBorrar.UseVisualStyleBackColor = true;
            this.BtnBorrar.Click += new System.EventHandler(this.BtnBorrar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 310);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Fecha Entrada";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 275);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Codigo";
            // 
            // dtgAlumnos
            // 
            this.dtgAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgAlumnos.Location = new System.Drawing.Point(156, 13);
            this.dtgAlumnos.Margin = new System.Windows.Forms.Padding(4);
            this.dtgAlumnos.Name = "dtgAlumnos";
            this.dtgAlumnos.Size = new System.Drawing.Size(439, 233);
            this.dtgAlumnos.TabIndex = 9;
            // 
            // TxtSalida
            // 
            this.TxtSalida.Location = new System.Drawing.Point(219, 337);
            this.TxtSalida.Margin = new System.Windows.Forms.Padding(4);
            this.TxtSalida.Name = "TxtSalida";
            this.TxtSalida.Size = new System.Drawing.Size(132, 22);
            this.TxtSalida.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(121, 342);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "Fecha Salida";
            // 
            // TXTHotel
            // 
            this.TXTHotel.Location = new System.Drawing.Point(219, 367);
            this.TXTHotel.Margin = new System.Windows.Forms.Padding(4);
            this.TXTHotel.Name = "TXTHotel";
            this.TXTHotel.Size = new System.Drawing.Size(132, 22);
            this.TXTHotel.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(167, 370);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "Hotel";
            // 
            // TXTParticipante
            // 
            this.TXTParticipante.Location = new System.Drawing.Point(219, 397);
            this.TXTParticipante.Margin = new System.Windows.Forms.Padding(4);
            this.TXTParticipante.Name = "TXTParticipante";
            this.TXTParticipante.Size = new System.Drawing.Size(132, 22);
            this.TXTParticipante.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(121, 402);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 17);
            this.label5.TabIndex = 22;
            this.label5.Text = "Participante";
            // 
            // FrmReserva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TXTParticipante);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TXTHotel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtSalida);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.BtnModificar);
            this.Controls.Add(this.BtnAgreagar);
            this.Controls.Add(this.TxtEntrada);
            this.Controls.Add(this.TxtId);
            this.Controls.Add(this.BtnBorrar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgAlumnos);
            this.Name = "FrmReserva";
            this.Text = "FrmReserva";
            this.Load += new System.EventHandler(this.FrmReserva_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Button BtnModificar;
        private System.Windows.Forms.Button BtnAgreagar;
        private System.Windows.Forms.TextBox TxtEntrada;
        private System.Windows.Forms.TextBox TxtId;
        private System.Windows.Forms.Button BtnBorrar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dtgAlumnos;
        private System.Windows.Forms.TextBox TxtSalida;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TXTHotel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TXTParticipante;
        private System.Windows.Forms.Label label5;
    }
}