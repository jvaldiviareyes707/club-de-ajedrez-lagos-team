﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejador;
using EntidadesAjedrez;

namespace Presentacion
{
    public partial class FrmMovimientos : Form
    {
        ManejadorMovimientos ma;
        public FrmMovimientos()
        {
            InitializeComponent();
            ma = new ManejadorMovimientos();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            
            Close();
        }

        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Borrar(new EntidadMovimientos(int.Parse(TxtIdpartida.Text),0, "", ""));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }
        void actualizar()
        {
            dtgAlumnos.DataSource = ma.Listado(string.Format("" +
                "select * from movimientos"), "movimientos").Tables[0];
            dtgAlumnos.AutoResizeColumns();
            dtgAlumnos.Columns[1].ReadOnly = true;
        }

        private void BtnAgreagar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Guardar(new EntidadMovimientos(int.Parse(TxtIdpartida.Text), int.Parse(TxtIDMovimiento.Text),TxtMovimiento.Text,TXTComentario.Text));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Modificar(new EntidadMovimientos(int.Parse(TxtIdpartida.Text),int.Parse(TxtMovimiento.Text), TxtMovimiento.Text,TXTComentario.Text));

                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void FrmMovimientos_Load(object sender, EventArgs e)
        {
            actualizar();
        }
    }
}
