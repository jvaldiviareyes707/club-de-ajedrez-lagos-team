﻿namespace Presentacion
{
    partial class FrmPartida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolver = new System.Windows.Forms.Button();
            this.BtnModificar = new System.Windows.Forms.Button();
            this.BtnAgreagar = new System.Windows.Forms.Button();
            this.TxtJornada = new System.Windows.Forms.TextBox();
            this.TxtId = new System.Windows.Forms.TextBox();
            this.BtnBorrar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtgAlumnos = new System.Windows.Forms.DataGridView();
            this.TXTArbitro = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtHotel = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtSala = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtEntradas = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(5, 13);
            this.btnVolver.Margin = new System.Windows.Forms.Padding(4);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(100, 28);
            this.btnVolver.TabIndex = 17;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // BtnModificar
            // 
            this.BtnModificar.Location = new System.Drawing.Point(497, 296);
            this.BtnModificar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.Size = new System.Drawing.Size(100, 28);
            this.BtnModificar.TabIndex = 16;
            this.BtnModificar.Text = "Modificar";
            this.BtnModificar.UseVisualStyleBackColor = true;
            this.BtnModificar.Click += new System.EventHandler(this.BtnModificar_Click);
            // 
            // BtnAgreagar
            // 
            this.BtnAgreagar.Location = new System.Drawing.Point(357, 311);
            this.BtnAgreagar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAgreagar.Name = "BtnAgreagar";
            this.BtnAgreagar.Size = new System.Drawing.Size(100, 28);
            this.BtnAgreagar.TabIndex = 15;
            this.BtnAgreagar.Text = "Agregar";
            this.BtnAgreagar.UseVisualStyleBackColor = true;
            this.BtnAgreagar.Click += new System.EventHandler(this.BtnAgreagar_Click);
            // 
            // TxtJornada
            // 
            this.TxtJornada.Location = new System.Drawing.Point(221, 307);
            this.TxtJornada.Margin = new System.Windows.Forms.Padding(4);
            this.TxtJornada.Name = "TxtJornada";
            this.TxtJornada.Size = new System.Drawing.Size(132, 22);
            this.TxtJornada.TabIndex = 14;
            // 
            // TxtId
            // 
            this.TxtId.Location = new System.Drawing.Point(221, 275);
            this.TxtId.Margin = new System.Windows.Forms.Padding(4);
            this.TxtId.Name = "TxtId";
            this.TxtId.Size = new System.Drawing.Size(132, 22);
            this.TxtId.TabIndex = 13;
            // 
            // BtnBorrar
            // 
            this.BtnBorrar.Location = new System.Drawing.Point(357, 275);
            this.BtnBorrar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnBorrar.Name = "BtnBorrar";
            this.BtnBorrar.Size = new System.Drawing.Size(100, 28);
            this.BtnBorrar.TabIndex = 12;
            this.BtnBorrar.Text = "Borrar";
            this.BtnBorrar.UseVisualStyleBackColor = true;
            this.BtnBorrar.Click += new System.EventHandler(this.BtnBorrar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(154, 308);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Jornada";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(158, 275);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Codigo";
            // 
            // dtgAlumnos
            // 
            this.dtgAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgAlumnos.Location = new System.Drawing.Point(158, 13);
            this.dtgAlumnos.Margin = new System.Windows.Forms.Padding(4);
            this.dtgAlumnos.Name = "dtgAlumnos";
            this.dtgAlumnos.Size = new System.Drawing.Size(439, 233);
            this.dtgAlumnos.TabIndex = 9;
            // 
            // TXTArbitro
            // 
            this.TXTArbitro.Location = new System.Drawing.Point(221, 337);
            this.TXTArbitro.Margin = new System.Windows.Forms.Padding(4);
            this.TXTArbitro.Name = "TXTArbitro";
            this.TXTArbitro.Size = new System.Drawing.Size(132, 22);
            this.TXTArbitro.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(154, 338);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "Arbitro";
            // 
            // TxtHotel
            // 
            this.TxtHotel.Location = new System.Drawing.Point(221, 367);
            this.TxtHotel.Margin = new System.Windows.Forms.Padding(4);
            this.TxtHotel.Name = "TxtHotel";
            this.TxtHotel.Size = new System.Drawing.Size(132, 22);
            this.TxtHotel.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(154, 368);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "Hotel";
            // 
            // TxtSala
            // 
            this.TxtSala.Location = new System.Drawing.Point(221, 397);
            this.TxtSala.Margin = new System.Windows.Forms.Padding(4);
            this.TxtSala.Name = "TxtSala";
            this.TxtSala.Size = new System.Drawing.Size(132, 22);
            this.TxtSala.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(154, 398);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 17);
            this.label5.TabIndex = 22;
            this.label5.Text = "Sala";
            // 
            // TxtEntradas
            // 
            this.TxtEntradas.Location = new System.Drawing.Point(221, 427);
            this.TxtEntradas.Margin = new System.Windows.Forms.Padding(4);
            this.TxtEntradas.Name = "TxtEntradas";
            this.TxtEntradas.Size = new System.Drawing.Size(132, 22);
            this.TxtEntradas.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(154, 428);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 17);
            this.label6.TabIndex = 24;
            this.label6.Text = "Entradas";
            // 
            // FrmPartida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 530);
            this.Controls.Add(this.TxtEntradas);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtSala);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtHotel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TXTArbitro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.BtnModificar);
            this.Controls.Add(this.BtnAgreagar);
            this.Controls.Add(this.TxtJornada);
            this.Controls.Add(this.TxtId);
            this.Controls.Add(this.BtnBorrar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgAlumnos);
            this.Name = "FrmPartida";
            this.Text = "Partida";
            this.Load += new System.EventHandler(this.FrmPartida_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgAlumnos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.Button BtnModificar;
        private System.Windows.Forms.Button BtnAgreagar;
        private System.Windows.Forms.TextBox TxtJornada;
        private System.Windows.Forms.TextBox TxtId;
        private System.Windows.Forms.Button BtnBorrar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dtgAlumnos;
        private System.Windows.Forms.TextBox TXTArbitro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtHotel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtSala;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtEntradas;
        private System.Windows.Forms.Label label6;
    }
}