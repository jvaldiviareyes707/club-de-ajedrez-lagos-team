﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejador;
using EntidadesAjedrez;

namespace Presentacion
{
    public partial class FrmPartida : Form
    {
        ManejadorPartida ma;
        public FrmPartida()
        {
            InitializeComponent();
            ma = new ManejadorPartida();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            Close();
        }
        void actualizar()
        {
            dtgAlumnos.DataSource = ma.Listado(string.Format("" +
                "select * from partida"), "partida").Tables[0];
            dtgAlumnos.AutoResizeColumns();
            dtgAlumnos.Columns[1].ReadOnly = true;
        }
        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Borrar(new EntidadPartida(int.Parse(TxtId.Text), "",0,0,0,0));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnAgreagar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Guardar(new EntidadPartida(0, TxtJornada.Text,int.Parse(TXTArbitro.Text),int.Parse(TxtHotel.Text),int.Parse(TxtSala.Text),int.Parse(TxtEntradas.Text)));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ma.Modificar(new EntidadPartida(int.Parse(TxtId.Text), TxtJornada.Text,int.Parse(TXTArbitro.Text),int.Parse(TxtHotel.Text),int.Parse(TxtSala.Text),int.Parse(TxtEntradas.Text)));

                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void FrmPartida_Load(object sender, EventArgs e)
        {
            actualizar();
        }
    }
}
