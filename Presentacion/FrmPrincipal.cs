﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmParticipantes a = new FrmParticipantes();
            a.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmHotel a = new FrmHotel();
            a.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmJuego a = new FrmJuego();
            a.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
          FrmMovimientos a = new FrmMovimientos();
            a.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FrmPais a = new FrmPais();
            a.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FrmPartida a = new FrmPartida();
            a.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FrmReserva a = new FrmReserva();
            a.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            FrmSala a = new FrmSala();
            a.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.Show();
        }
    }
}
