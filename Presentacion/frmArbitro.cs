﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntidadesAjedrez;
using Manejador;

namespace Presentacion
{
    public partial class frmArbitro : Form
    {
        ManejadorArbitro mc;
        public frmArbitro()
        {
            
            InitializeComponent();
            mc = new ManejadorArbitro();
        }
        void actualizar()
        {
            dataGridView1.DataSource = mc.Listado(string.Format("" +
                "select * from arbitro"), "arbitro").Tables[0];
            dataGridView1.AutoResizeColumns();
            dataGridView1.Columns[1].ReadOnly = true;
        }
        private void btnborrar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mc.Borrar(new EntidadArbitro(int.Parse(TxtCodigo.Text), 0));               
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mc.Guardar(new EntidadArbitro(0, int.Parse(TxtAlumno.Text)));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");

            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mc.Modificar(new EntidadArbitro(int.Parse(TxtCodigo.Text), int.Parse(TxtAlumno.Text)));
                //Close();
                actualizar();
            }
            catch (Exception)
            {

                MessageBox.Show("Error de dato.");
            }
        }

        private void frmArbitro_Load(object sender, EventArgs e)
        {
            actualizar();
        }

        private void btnvolver_Click(object sender, EventArgs e)
        {
            FrmParticipantes a = new FrmParticipantes();
            a.Show();
            Close();
        }
    }
}
